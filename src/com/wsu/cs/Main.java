package com.wsu.cs;

import java.util.LinkedList;
/**
 * Creates Linked List of Items at a Grocery Store
 * - Perishable and Miscellaneous Items
 *
 * @author S Nagpal
 * Version 2020
 */
public class Main {

public static void main(String[] args) {

    LinkedList<GroceryStore> GroceryList = new LinkedList();

    /*
    Declare 4 objects of PerishableItem
    Declare 4 objects of MiscellaneousItem
    Add them to the GroceryList using different methods given in the table of POGIL Activity3.
    Also, make sure that set and remove methods are used,
    such that after using each of the 8 objects created at least once,
    at the end of all statements, the size of GroceryList is 6.
     */

    // Perishable Items
    PerishableItem p1 = new PerishableItem("Milk",2, 10, 11, 2020);
    PerishableItem p2 = new PerishableItem("Cream",2, 11, 22, 2020);
    PerishableItem p3 = new PerishableItem("Eggs",1, 10, 18, 2020);
    PerishableItem p4 = new PerishableItem("Chicken",3, 10, 3, 2020);

    // Miscellaneous Item
    MiscellaneousItem m1 = new MiscellaneousItem("Canned Tuna", 5, 2020);
    MiscellaneousItem m2 = new MiscellaneousItem("Bottled Water", 3, 2020);
    MiscellaneousItem m3 = new MiscellaneousItem("Olive Oil", 1, 2020);
    MiscellaneousItem m4 = new MiscellaneousItem("Rice", 5, 2020);

    // Add Perishable Items
    GroceryList.add(p1);
    GroceryList.add(p2);
    GroceryList.add(2, p3);
    GroceryList.add(3, p4);

    // Add Miscellaneous Items
    GroceryList.add(1, m1);
    GroceryList.add(1, m2);
    GroceryList.add(m3);
    GroceryList.add(m4);

    // Remove Items
    GroceryList.remove(0);
    GroceryList.remove(GroceryList.size() - 1);

    // Print List
    for (GroceryStore grocery : GroceryList)
        System.out.println(grocery.toString());

  }

}
